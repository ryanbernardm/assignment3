package assign.domain;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "output")
@XmlAccessorType(XmlAccessType.FIELD)
public class Error {
		
	List<String> error = null;

	public List<String> getError() {
        return error;
    }
 
    public void setError(List<String> link) {
        this.error = link;
    }
}
