package assign.services;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;



public class EavesdropService {

	//return all projects from eavesdrop page
	public Elements getAllProjects(String url) throws IOException {
		Document document = Jsoup.connect(url).get();
		Elements links = document.select("body a");
		return links;
	}
	
	//get all meetings for given project name
	public Elements getMeetings(String url, String project) throws IOException{
		Document document = Jsoup.connect(url).get();
		Elements links = document.getElementsByAttributeValueMatching("href", project + "/");
		
		//ensure project exists on website
		if(links == null || links.isEmpty()){
			return null;
		}
		document = Jsoup.connect(links.attr("abs:href")).get();  //might be doing wrong connect
        links = document.select("body a");
        return links;
	}
}