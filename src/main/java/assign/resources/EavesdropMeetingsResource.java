package assign.resources;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.jsoup.select.Elements;

import assign.domain.Meetings;
import assign.domain.Error;
import assign.domain.Projects;
import assign.services.EavesdropService;

@Path("/projects")
public class EavesdropMeetingsResource {
	
	EavesdropService eavesdropService;
	private final String url = "http://eavesdrop.openstack.org/meetings";
	
	public EavesdropMeetingsResource() {
		this.eavesdropService = new EavesdropService();
	}
	
	@GET
	@Path("/")
	@Produces("application/xml")
	//use eavesdrop service to return all projects on webpage
	public StreamingOutput getAllProjects() throws Exception {
		final Projects projects = new Projects();
		Elements links = null;
		projects.setProjects(new ArrayList<String>());
		links = eavesdropService.getAllProjects(url);
		
		//skip first 5 elements due to webpage layout
		for(int i = 5; i < links.size(); i++){
			projects.getProjects().add(links.get(i).html());
		}
		return new StreamingOutput() {
		    public void write(OutputStream outputStream) throws IOException, WebApplicationException {
		    	outputProjects(outputStream, projects);
		    }
		};    
	}	
	
	
	@GET
	@Path("/{project}/meetings")
	@Produces("application/xml")
	//use eavesdrop service to return all meetings for a given project
	//return error if project does not exist on webpage
	public StreamingOutput getMeetings(@PathParam("project") String project) throws Exception {
		final Meetings meetings = new Meetings();
		final Error err = new Error();
		meetings.setMeetings(new ArrayList<String>());
		err.setError(new ArrayList<String>());
		Elements links = null;
		links = eavesdropService.getMeetings(url, project);
		if(links == null){
			err.getError().add("Project "+ project + " does not exist");
			return new StreamingOutput() {
			    public void write(OutputStream outputStream) throws IOException, WebApplicationException {
			    	outputError(outputStream, err);
			    }
			};
		}
		
		//skip first 5 elements due to webpage layout
		for(int i = 5; i < links.size(); i++){
			meetings.getMeetings().add(links.get(i).html());
		}
		return new StreamingOutput() {
		    public void write(OutputStream outputStream) throws IOException, WebApplicationException {
		    	outputMeetings(outputStream, meetings);
		    }
		};
	}
	
	protected void outputProjects(OutputStream os, Projects projects) throws IOException {
		try { 
			JAXBContext jaxbContext = JAXBContext.newInstance(Projects.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(projects, os);
		} catch (JAXBException jaxb) {
			jaxb.printStackTrace();
			throw new WebApplicationException();
		}
	}
	
	protected void outputMeetings(OutputStream os, Meetings meetings) throws IOException {
		try { 
			JAXBContext jaxbContext = JAXBContext.newInstance(Meetings.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(meetings, os);
		} catch (JAXBException jaxb) {
			jaxb.printStackTrace();
			throw new WebApplicationException();
		}
	}
	
	protected void outputError(OutputStream os, Error err) throws IOException {
		try { 
			JAXBContext jaxbContext = JAXBContext.newInstance(Error.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(err, os);
		} catch (JAXBException jaxb) {
			jaxb.printStackTrace();
			throw new WebApplicationException();
		}
	}
}

