package assign.resources;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.Before;
import org.junit.Test;


public class TestEavesdropMeetingsResource
{

   private HttpClient client;

   @Before
   public void initClient()
   {
	   client = HttpClientBuilder.create().build();
   }
   
   @Test
   public void test1() throws Exception{
		
	   ArrayList<String> actual = new ArrayList<String>();
	   System.out.println("Test getAllProjects");
	   String url = "http://localhost:8080/assignment3/myeavesdrop/projects/";
	
	   HttpGet request = new HttpGet(url);
	   HttpResponse check = client.execute(request);
	   System.out.println("Response Code : "+ check.getStatusLine().getStatusCode());
	   BufferedReader rd = new BufferedReader(new InputStreamReader(check.getEntity().getContent()));
	   StringBuffer result = new StringBuffer();
	   String line = "";
	   while ((line = rd.readLine()) != null) {
		   result.append(line);
	   }
	   System.out.println(result);
		
	   Document document = Jsoup.parse(result.toString());
	   for(Element link : document.select("project")){
		   String temp = link.html();
		   actual.add(temp);
	   }
	   try{
		   assertTrue("Test Failed", (actual.contains("3rd_party_ci/") && actual.contains("17_12_2015/") && actual.contains("2015_09_17/")
				   && actual.contains("2015_10_15/") && actual.contains("2015_10_29/") && actual.contains("____freezer_meeting_23_07_2015____/")
				   && actual.contains("___endmeeting/") && actual.contains("__dragonflow_/") && actual.contains("__ironic_neutron/") && 
				   actual.contains("__networking_l2gw/")));
		   System.out.println("Test passed!");
	   }
	   catch(AssertionError ae){
		   ae.printStackTrace();
	   }
   }
	   
	   
   @Test
   public void test2() throws Exception{
	 
		ArrayList<String> actual = new ArrayList<String>();
		ArrayList<String> yearsRequired = new ArrayList<String>();
		yearsRequired.add("2013/");
		yearsRequired.add("2014/");
		yearsRequired.add("2015/");
		yearsRequired.add("2016/");
		yearsRequired.add("2017/");
	
	
		System.out.println("Test Solum Team Meeting");
	   	String url = "http://localhost:8080/assignment3/myeavesdrop/projects/solum_team_meeting/meetings";
	
	   	HttpGet request = new HttpGet(url);
	
		HttpResponse check = client.execute(request);
	
		System.out.println("Response Code : "+ check.getStatusLine().getStatusCode());
	
		BufferedReader rd = new BufferedReader(new InputStreamReader(check.getEntity().getContent()));
	
		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
	
		System.out.println(result);
		Document document = Jsoup.parse(result.toString());
		for(Element link : document.select("year")){
			String temp = link.html();
			actual.add(temp);
		}
		try{
			assertEquals("Test Failed", yearsRequired, actual);
			System.out.println("Test passed!");
		}
		catch(AssertionError ae){
			ae.printStackTrace();
		}
   }
   
   @Test
   public void test3() throws Exception{
	 
		ArrayList<String> actual = new ArrayList<String>();
		ArrayList<String> yearsRequired = new ArrayList<String>();
		yearsRequired.add("2014/");
	
		System.out.println("Test 3rd party ci meetings");
	   	String url = "http://localhost:8080/assignment3/myeavesdrop/projects/3rd_party_ci/meetings";
	
	   	HttpGet request = new HttpGet(url);
	
		HttpResponse check = client.execute(request);
	
		System.out.println("Response Code : "+ check.getStatusLine().getStatusCode());
	
		BufferedReader rd = new BufferedReader(new InputStreamReader(check.getEntity().getContent()));
	
		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
	
		System.out.println(result);
		Document document = Jsoup.parse(result.toString());
		for(Element link : document.select("year")){
			String temp = link.html();
			actual.add(temp);
		}
		try{
			assertEquals("Test Failed", yearsRequired, actual);
			System.out.println("Test passed!");
		}
		catch(AssertionError ae){
			ae.printStackTrace();
		}
   }
   
   @Test
   public void test4() throws Exception{
	 
		ArrayList<String> actual = new ArrayList<String>();
		ArrayList<String> stringRequired = new ArrayList<String>();
		stringRequired.add("Project non-existent-project does not exist");
	
		System.out.println("Test non existent project");
	   	String url = "http://localhost:8080/assignment3/myeavesdrop/projects/non-existent-project/meetings";
	
	   	HttpGet request = new HttpGet(url);
	
		HttpResponse check = client.execute(request);
	
		System.out.println("Response Code : "+ check.getStatusLine().getStatusCode());
	
		BufferedReader rd = new BufferedReader(new InputStreamReader(check.getEntity().getContent()));
	
		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
	
		System.out.println(result);
		Document document = Jsoup.parse(result.toString());
		for(Element link : document.select("error")){
			String temp = link.html();
			actual.add(temp);
		}
		try{
			assertEquals("Test Failed", stringRequired, actual);
			System.out.println("Test passed!");
		}
		catch(AssertionError ae){
			ae.printStackTrace();
		}
   }
}